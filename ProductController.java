package Product.src.test.java.mentorama.com.Product;

public class ProductController {

    Cadeia de caracteres final estática privada  MOCKED_RESULT="{\"product\"]}";

    privado estático WireMockServer wireMockServer= novo WireMockServer(options().porta(8081));

    @Com conexão automática
    privado MockMvc mockMvc ;

    @Com conexão automática
    privado ObjectMapper objectMapper ;

    @AntesTudo
    static void beforeAll() lança JacksonException {
        wireMockServer. começar();
    }

    @AntesCada
    configuração void(){
        wireMockServer. resetAll();
    }

    @Afinal
    vazio estático  depois de tudo(){
        wireMockServer. parar();
    }

    @Teste
    public void shouldFindAllProducts() lança Exception{
        wireMockServer. stubPara(
                WireMock. get(WireMock.urlPathEqualTo("/produto"))
                        . willReturn(aResposta()
                                . comStatus(200)
                                . withHeader("Content-Type", "aplicattion/json")
                                . comCorpo(MOCKED_RESULT)));
        ResultActions resultActions  = mockMvc. executar(
                        get("/produto"). contentType(MediaType.APPLICATION_JSON))
                . eExpect(status().isOk())
                . andExpect(jsonPath("$",hasSize(4)));


    }
}

