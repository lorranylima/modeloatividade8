package Product.src.test.java.mentorama.com.Product;

public class ProdutcTeste {
    @Teste
    vazio público  deveCalcularPreçoTotalComDesconto(){

        Produto = novo Produto(1,
                "Arcondicionado",
                3º,
                3000,00,
                0.1);

        Duplo resultado = produto. getPriceWithDiscount(0.10);
        assertEquals(2700.00 , resultado);
    }

    @Teste
    vazio público
    whenDiscountIsHigherThanMaxDiscountShouldUseMaxDiscountPercentage(){

        Produto = novo Produto(1,
                "Arcondicionado" +
                        "" +
                3º,
                3000,00,
                0.1);

        Duplo resultado = produto. getPriceWithDiscount(0.15);
        assertEquals(2700.00 , resultado);
    }

    @Teste
    vazio público
    whenDiscountIsLowerThanMaxDiscountShouldUseMaxDiscountPercentage(){

        Produto = novo Produto(1,
                "Arcondicionado",
                3º,
                3000,00,
                0.1);

        Duplo resultado = produto. getPriceWithDiscount(0.05);
        assertEquals(2850.00 , resultado);
    }

    @Teste
    vazio público  deveAddQuantityIsCorretct(){

        Produto = novo Produto(1,
                "Arcondicionado",
                3º,
                3000,00,
                0.1);

        Resultado inteiro  = produto. AddQuantity(2);
        assertEquals(5 , resultado);
    }

    @Teste
    vazio público
    whenQuantityToReduceIsHigherThanQuantityInStock(){

        Produto = novo Produto(1,
                "Arcondicionado",
                3º,
                3000,00,
                0.1);

        Resultado inteiro  = produto. ReduceQuantity(4);
        assertEquals(0 , resultado);
    }

    @Teste
    vazio público
    whenQuantityToReduceIsLowerThanQuantityInStock(){

        Produto = novo Produto(1,
                "Arcondicionado",
                3º,
                3000,00,
                0.1);

        Resultado inteiro  = produto. ReduceQuantity(2);
        assertEquals(1 , resultado);
    }

}
}
