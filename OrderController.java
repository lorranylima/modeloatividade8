package Product.src.test.java.mentorama.com.Product;

public class OrderController {

    String final estática privada  MOCKED_RESULT="" + "{\"order\":[\n"+
            "{\n"+
            "{\"produto\":\n"+
            "\"id\":01,\n"+
            "\"nome\":\"TV\",\n"+
            "\"quantidadeInStock\":3,\n"+
            "\"preço\":2000.00,\n"+
            "\"maxDiscountPercentage\":0.9\n"+
            "},\n"+
            "{\n"+
            "\"quantidade\":2,\n"+
            "\"desconto\":0.1\n"+
            "},\n"+
            "]}";

    privado estático WireMockServer wireMockServer= novo WireMockServer(options().porta(8081));

    @Com conexão automática
    privado MockMvc mockMvc ;

    @Com conexão automática
    privado ObjectMapper objectMapper ;

    @AntesTudo
    static void beforeAll() lança JacksonException {
        wireMockServer. começar();
    }

    @AntesCada
    configuração void(){
        wireMockServer. resetAll();
    }

    @Afinal
    vazio estático  depois de tudo(){
        wireMockServer. parar();
    }

    @Teste
    public void shouldDoOrderCalculate() lança Exception{
        wireMockServer. stubPara(
                WireMock. get(WireMock.urlPathEqualTo("/ordem"))
                        . willReturn(aResposta()
                                . comStatus(200)
                                . withHeader("Content-Type", "aplicattion/json")
                                . comCorpo(MOCKED_RESULT)));
        ResultActions resultActions  = mockMvc. executar(
                        get("/ordem"). contentType(MediaType.APPLICATION_JSON))
                . eExpect(status().isOk())
                . eExpect(jsonPath("$",hasValue(5400.00)));


    }
}

